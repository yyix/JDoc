package com.jdocapi.doc.core.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jdocapi.doc.bean.Api;
import com.jdocapi.doc.bean.ApiRequestParam;
import com.jdocapi.doc.bean.ApiResponseParam;
import com.jdocapi.doc.core.constant.Constant;
import com.jdocapi.doc.core.exception.JDocException;
import com.jdocapi.doc.core.parser.BeanDocumentParser;
import com.jdocapi.doc.core.parser.BeanParamContainer;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.SeeTag;
import com.sun.javadoc.Tag;

public abstract class AbstractApiParser {

    /**
     * 过滤出Controller文档
     * 
     * @param classDocs
     * @return
     */
    public abstract List<String> filterController(List<String> fileNames);

    public abstract List<Api> generateApi(ClassDoc[] classDocs);

    public List<ApiRequestParam> generateApiReqeustParam(Tag[] tags) {
        List<ApiRequestParam> apiRequestParams = new ArrayList<ApiRequestParam>();
        List<Tag> paramTags = getTagsByTag(tags, "@param");
        List<Tag> paramBeanTags = getTagsByTag(tags, "@paramBean");
        for (Tag tag : paramTags) {
            ApiRequestParam apiRequestParam = new ApiRequestParam();
            String tagText = tag.text();
            String[] descs = tagText.split("\\|");
            if (descs.length < 4) {
                throw new JDocException("the param formater dose not match jdoc formater: " + tagText);
            }

            apiRequestParam.setName(descs[0]);
            apiRequestParam.setDesc(descs[1]);
            apiRequestParam.setType(descs[2]);
            apiRequestParam.setRequired(descs[3]);
            apiRequestParams.add(apiRequestParam);
        }

        if (paramBeanTags != null && !paramBeanTags.isEmpty()) {
            generateSeeTag(tags);
            for (Tag paramBeanTag : paramBeanTags) {
                String beanName = paramBeanTag.text();
                List<ApiRequestParam> beanApiRequestParams = BeanParamContainer.getApiRequestParamParam(beanName);
                if(beanApiRequestParams==null){
                    throw new JDocException("not find "+beanName+" bean on see tag");
                }
                apiRequestParams.addAll(beanApiRequestParams);
            }
        }

        return apiRequestParams;
    }

    public List<ApiResponseParam> generateApiResponseParam(Tag[] tags) {
        List<ApiResponseParam> apiResponseParams = new ArrayList<ApiResponseParam>();
        List<Tag> paramTags = getTagsByTag(tags, "@respParam");
        List<Tag> respParamBeanTags = getTagsByTag(tags, "@respParamBean");
        for (Tag tag : paramTags) {
            ApiResponseParam apiResponseParam = new ApiResponseParam();
            String tagText = tag.text();
            String[] descs = tagText.split("\\|");
            if (descs.length < 4) {
                throw new JDocException("the respParam formater dose not match jdoc formater: " + tagText);
            }

            apiResponseParam.setName(descs[0]);
            apiResponseParam.setDesc(descs[1]);
            apiResponseParam.setType(descs[2]);
            apiResponseParam.setRequired(descs[3]);

            apiResponseParams.add(apiResponseParam);
        }
        if (respParamBeanTags != null && !respParamBeanTags.isEmpty()) {
            generateSeeTag(tags);
            for (Tag paramBeanTag : respParamBeanTags) {
                String beanName = paramBeanTag.text();
                List<ApiResponseParam> beanapiResponseParams = BeanParamContainer.getApiResponseParam(beanName);
                if(beanapiResponseParams==null){
                    throw new JDocException("not find "+beanName+" bean on see tag");
                }
                apiResponseParams.addAll(beanapiResponseParams);
            }
        }
        return apiResponseParams;
    }

    public String getTitle(Tag[] tags) {
        List<Tag> targetTags = getTagsByTag(tags, "@title");
        if (targetTags != null && !targetTags.isEmpty()) {
            return targetTags.get(0).text();
        }

        return "";
    }
    
    public String getAuthor(Tag[] tags) {
        List<Tag> targetTags = getTagsByTag(tags, "@author");
        if (targetTags != null && !targetTags.isEmpty()) {
            return targetTags.get(0).text();
        }

        return "";
    }

    public String getRespBody(Tag[] tags) {
        List<Tag> targetTags = getTagsByTag(tags, "@respBody");
        if (targetTags != null && !targetTags.isEmpty()) {
            return targetTags.get(0).text();
        }

        return "";
    }

    public void generateSeeTag(Tag[] tags) {
        List<Tag> targetTags = getTagsByTag(tags, "@see");
        List<String> beanJavaFileNames = new ArrayList<String>();
        for (Tag tag : targetTags) {
            SeeTag seeTag = (SeeTag) tag;
            String className = seeTag.referencedClassName();
            beanJavaFileNames.add(Constant.JAVA_FILE_PATH + className.replaceAll("\\.", "/") + Constant.JAVA_FILE_SUFFIX);
        }
        if (!beanJavaFileNames.isEmpty()) {
            beanJavaFileNames.add(0, "-doclet");
            beanJavaFileNames.add(1, BeanDocumentParser.class.getName());
            String[] docArgs = beanJavaFileNames.toArray(new String[beanJavaFileNames.size()]);
            com.sun.tools.javadoc.Main.execute(docArgs);
        }
    }

    public List<Tag> getTagsByTag(Tag[] tags, String tagName) {
        List<Tag> targetTags = new ArrayList<Tag>();
        for (Tag tag : tags) {
            if (StringUtils.equals(tag.name(), tagName)) {
                targetTags.add(tag);
            }
        }
        return targetTags;
    }
}
